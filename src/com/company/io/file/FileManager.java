package com.company.io.file;

import com.company.model.Library;

public interface FileManager {
    Library importData();
    void exportData(Library library);
}
