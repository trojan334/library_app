package com.company.exeption;

public class NoSuchOptionException extends Exception {
    // its only for practice (i know that in Java is NoSuchElementException)
    public NoSuchOptionException(String message) {
        super(message);
    }
}
