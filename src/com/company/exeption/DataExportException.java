package com.company.exeption;

public class DataExportException extends RuntimeException {
    public DataExportException(String message) {
        super(message);
    }
}
