package com.company.exeption;

public class PublicationAlreadyExistsException extends RuntimeException{
    public PublicationAlreadyExistsException(String message){
        super(message);
    }
}
